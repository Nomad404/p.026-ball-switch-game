# Ball Switch Game

This prototype was made for the goal to create a small, mobile game on a grid with moving spheres.

## Rules

The game is simple.

Your goal as the player is to clear the grid of balls.

Each column can only let through balls with a specific color.

You can swap neighboring balls horizontally to sort them to their corresponding columns.  

Special balls:
- Rainbow: Can go through any colored column
- Freeze (snowflake): Stops the queue for a couple seconds after being destroyed
- Laser (horizontal line): Removes a whole row after a couple of seconds
- Bomb: Removes a block of balls around itself
- No-switch (lock): Can't be moved, falls through in any column
- Block-ball (red X): Can't fall through, has to be destroyed by a bomb/laser

## Screenshots

![Main Game](Images/MainGame.jpg)
<style type="text/css">
    img {
        width: 250px;
    }
</style>

## Resources

- [Grid generation by Code Monkey](https://www.youtube.com/watch?v=waEsGu--9P8)
- [Grid generation by Tarodev](https://www.youtube.com/watch?v=kkAjpQAM-jE)