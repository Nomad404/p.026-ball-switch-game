using System.Collections;
using BallSwitchGame.Controllers;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace BallSwitchGame.Tests.PlayMode
{
    public class LevelSetupTests
    {
        [SetUp]
        public void Setup()
        {
            SceneManager.LoadScene(LevelNames.MainGame);
        }

        [UnityTest]
        public IEnumerator LevelSetupTest()
        {
            yield return new WaitForLevelState(LevelState.StartLevel);

            var ballGridController = BallGridController.Instance;
            // Check that the grid is full
            Assert.IsEmpty(ballGridController.GetFreeColumnIndices());

            // Check that queue is not empty
            var queueController = QueueController.Instance;
            Assert.NotZero(queueController.BallQueue.Count);
        }

        [UnityTest]
        public IEnumerator BallGridSwapTest()
        {
            yield return new WaitForLevelState(LevelState.StartLevel);

            var ballGridController = BallGridController.Instance;
            var ballGrid = ballGridController.BallGrid;

            var fromPos = new Vector2Int(0, 1);
            var toPos = new Vector2Int(1, 1);
            var fromId = ballGrid[fromPos.x, fromPos.y];
            var toId = ballGrid[toPos.x, toPos.y];

            // Swap balls
            ballGridController.MoveBallOnGridTo(fromPos, toPos);

            Assert.AreEqual(fromId, ballGrid[toPos.x, toPos.y]);
            Assert.AreEqual(toId, ballGrid[fromPos.x, fromPos.y]);
        }
    }
}