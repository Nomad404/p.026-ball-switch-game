namespace BallSwitchGame.Static
{
    public static class SpriteLayers
    {
        public const int Default = -1;
        public const int Background = 0;

        public const int QueueTiles = Background + 1;
        public const int QueueBalls = QueueTiles + 1;
        public const int GridTiles = QueueBalls + 5;
        public const int Balls = GridTiles + 1;
    }
}