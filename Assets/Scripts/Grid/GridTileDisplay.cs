using BallSwitchGame.Static;
using UnityEngine;

namespace BallSwitchGame.Grid
{
    /// <summary>
    /// Visual representation of a grid tile
    /// </summary>
    [RequireComponent(typeof(SpriteRenderer))]
    internal class GridTileDisplay : MonoBehaviour
    {
        public Vector3 TileSize => GetComponent<Renderer>().bounds.size;

        private int _sortingOrder = SpriteLayers.GridTiles;

        public int SortingOrder
        {
            get => _sortingOrder;
            set
            {
                if (value == _sortingOrder) return;
                _sortingOrder = value;
                UpdateSortingOrder();
            }
        }

        public Color TileColor
        {
            get
            {
                if (_spriteRenderer == null) _spriteRenderer = GetComponent<SpriteRenderer>();
                return _spriteRenderer.color;
            }
            set
            {
                if (_spriteRenderer == null) _spriteRenderer = GetComponent<SpriteRenderer>();
                if (value == _spriteRenderer.color) return;
                _spriteRenderer.color = value;
            }
        }

        private SpriteRenderer _spriteRenderer;

        public GridTileDisplay(Vector2Int gridPosition)
        {
        }

        private void Awake()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            UpdateSortingOrder();
        }

        private void UpdateSortingOrder()
        {
            if (_spriteRenderer == null) _spriteRenderer = GetComponent<SpriteRenderer>();
            _spriteRenderer.sortingOrder = SortingOrder;
        }
    }
}