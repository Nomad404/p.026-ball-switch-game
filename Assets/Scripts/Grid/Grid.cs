using System;
using UnityEngine;

namespace BallSwitchGame.Grid
{
    /// <summary>
    /// Abstract representation of a grid
    /// </summary>
    internal class Grid
    {
        public readonly Vector2Int GridSize;

        public readonly GridTileDisplay[,] Tiles;

        public Grid(Vector2Int gridSize, Func<Vector2Int, GridTileDisplay> createGridCell)
        {
            GridSize = gridSize;
            Tiles = new GridTileDisplay[GridSize.x, GridSize.y];
            for (var x = 0; x < Tiles.GetLength(0); x++)
            {
                for (var y = 0; y < Tiles.GetLength(1); y++)
                {
                    var gridPos = new Vector2Int(x, y);
                    var cell = createGridCell.Invoke(gridPos);
                    Tiles[x, y] = cell;
                }
            }
        }
    }
}