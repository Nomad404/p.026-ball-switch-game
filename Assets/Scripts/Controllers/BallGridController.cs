using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using BallSwitchGame.Dynamic;
using BallSwitchGame.Grid;
using BallSwitchGame.Static;
using UnityEngine;
using Random = UnityEngine.Random;

[assembly: InternalsVisibleTo("PlayMode")]

namespace BallSwitchGame.Controllers
{
    internal class BallGridController : MonoBehaviour
    {
        private static BallGridController _instance;

        public static BallGridController Instance
        {
            get
            {
                if (_instance == null) _instance = FindObjectOfType<BallGridController>();
                return _instance;
            }
        }

        private readonly List<BallType> _baseBallTypes = new()
            {BallType.Red, BallType.Green, BallType.Blue, BallType.Yellow};

        [Header("Ball prefabs")] public BallDisplay ballDisplayPrefab;

        public List<BallDisplay> specialBallPrefabs;

        public Guid[,] BallGrid { get; private set; }

        private readonly List<BallDisplay> _ballPool = new();

        private void Awake()
        {
            var gridSize = TileGridController.Instance.gridSize;
            BallGrid = new Guid[gridSize.x, gridSize.y];
        }

        public void AddBallToGrid(BallDisplay ball)
        {
            var baseGrid = TileGridController.Instance.Grid.Tiles;
            var columnColors = TileGridController.Instance.columnColors;
            var startTile = TileGridController.Instance.StartTileDisplay;
            ball.QueueIndex = -1;
            ball.SortingOrder = SpriteLayers.Balls;

            // Check if grid has space to add balls
            var x = GetRandomFreeColumnIndex();

            // Get a different index if it's the first ball in a column
            if (x >= 0 && BallGrid[x, 0] == Guid.Empty && columnColors[x] == ball.Type)
            {
                x = GetRandomFreeColumnIndex(x);
            }

            if (x < 0) return;

            // Move ball down to the last free position on the grid
            var gridPath = GetFreeGridPathFromTop(x);
            var movePath = new List<Vector3> {startTile.transform.position};
            movePath.AddRange(gridPath.Select(gridPos => baseGrid[gridPos.x, gridPos.y].transform.position));
            ball.MoveOnPathOverTime(movePath, 0.05f);
            var lastPos = gridPath.Last();
            BallGrid[lastPos.x, lastPos.y] = ball.ID;
            ball.GridPosition = new Vector2Int(lastPos.x, lastPos.y);
            ball.WorldOriginPosition = movePath.Last();
        }

        public List<int> GetFreeColumnIndices()
        {
            var result = new List<int>();
            if (BallGrid.GetLength(0) <= 0) return result;
            var y = BallGrid.GetLength(1) - 1;
            for (var x = 0; x < BallGrid.GetLength(0); x++)
            {
                if (BallGrid[x, y] != Guid.Empty) continue;
                result.Add(x);
            }

            return result;
        }

        private int GetRandomFreeColumnIndex(int excludeIndex = -1)
        {
            var indices = GetFreeColumnIndices();
            if (excludeIndex >= 0) indices = indices.Where(i => i != excludeIndex).ToList();
            if (indices.Count == 0) return -1;
            var random = Random.Range(0, indices.Count);
            return indices[random];
        }

        private List<Vector2Int> GetFreeGridPathFromTop(int x)
        {
            var result = new List<Vector2Int>();
            var y = BallGrid.GetLength(1) - 1;
            while (BallGrid[x, y] == Guid.Empty)
            {
                result.Add(new Vector2Int(x, y));
                y--;
                if (y < 0) break;
            }

            return result;
        }

        public BallDisplay CreateRandomBall(bool includeSpecials = false)
        {
            var types = new List<BallType>(_baseBallTypes);
            if (includeSpecials) types.Add(BallType.Special);
            var randomType = types[Random.Range(0, types.Count)];
            var ballPrefab = randomType == BallType.Special
                ? specialBallPrefabs[Random.Range(0, specialBallPrefabs.Count)]
                : ballDisplayPrefab;

            var ball = Instantiate(ballPrefab, transform, true);
            ball.Type = randomType;
            ball.GridSize = new Vector2Int(BallGrid.GetLength(0), BallGrid.GetLength(1));
            _ballPool.Add(ball);
            return ball;
        }

        public bool MoveBallOnGridTo(Vector2Int from, Vector2Int to)
        {
            if (from == to) return false;
            var baseGrid = TileGridController.Instance.Grid.Tiles;

            var fromId = BallGrid[from.x, from.y];
            var toId = BallGrid[to.x, to.y];
            var fromTile = baseGrid[from.x, from.y];
            var toTile = baseGrid[to.x, to.y];

            if (fromId == Guid.Empty) return false;
            var fromBall = GetBallFromPoolById(_ballPool, fromId);

            // Balls can also be moved to empty tiles
            if (toId != Guid.Empty)
            {
                var toBall = GetBallFromPoolById(_ballPool, toId);
                // Executed before to prevent unnecessary swaps
                if (toBall is NoSwitchBall) return false;
                toBall.GridPosition = from;
                toBall.WorldOriginPosition = fromTile.transform.position;
                BallGrid[from.x, from.y] = toBall.ID;
                toBall.MoveToOverTime(toBall.WorldOriginPosition, 0.1f);
            }
            else
            {
                BallGrid[from.x, from.y] = Guid.Empty;
            }

            // Executed after to prevent unnecessary swaps
            fromBall.GridPosition = to;
            fromBall.WorldOriginPosition = toTile.transform.position;
            BallGrid[to.x, to.y] = fromBall.ID;
            fromBall.MoveToOverTime(fromBall.WorldOriginPosition, 0.1f);

            CheckAllColumnsForBallCombos();
            QueueController.Instance.MoveUpQueuedBalls();
            return true;
        }

        public async void DestroyRowAt(int y)
        {
            for (var x = 0; x < BallGrid.GetLength(0); x++)
            {
                var ballId = BallGrid[x, y];
                if (ballId == Guid.Empty) continue;
                var ball = GetBallFromPoolById(_ballPool, ballId);
                _ballPool.Remove(ball);
                BallGrid[x, y] = Guid.Empty;
                Destroy(ball.gameObject);
            }

            await Task.Delay(500);

            CheckAllColumnsForBallCombos();
            QueueController.Instance.MoveUpQueuedBalls();
        }

        public async void DestroyBlockAt(Vector2Int pos)
        {
            var xMin = Mathf.Max(pos.x - 1, 0);
            var xMax = Mathf.Min(pos.x + 1, BallGrid.GetLength(0) - 1);
            var yMin = Mathf.Max(pos.y - 1, 0);
            var yMax = Mathf.Min(pos.y + 1, BallGrid.GetLength(1) - 1);
            for (var x = xMin; x <= xMax; x++)
            {
                for (var y = yMin; y <= yMax; y++)
                {
                    var ballId = BallGrid[x, y];
                    if (ballId == Guid.Empty) continue;
                    var ball = GetBallFromPoolById(_ballPool, ballId);
                    _ballPool.Remove(ball);
                    BallGrid[x, y] = Guid.Empty;
                    Destroy(ball.gameObject);
                }
            }

            await Task.Delay(500);

            CheckAllColumnsForBallCombos();
            QueueController.Instance.MoveUpQueuedBalls();
        }

        private void CheckAllColumnsForBallCombos()
        {
            var baseGrid = TileGridController.Instance.Grid.Tiles;
            var columnColors = TileGridController.Instance.columnColors;
            if (columnColors.Length == 0) return;
            var endPos = TileGridController.Instance.EndTileDisplay.transform.position;
            // Move across each column
            for (var x = 0; x < BallGrid.GetLength(0); x++)
            {
                RemoveBallComboAtColumn(x, baseGrid, BallGrid, endPos, _ballPool, columnColors);
                MoveBallsToBottomAtColumn(x, baseGrid, BallGrid, _ballPool);
                RemoveBallComboAtColumn(x, baseGrid, BallGrid, endPos, _ballPool, columnColors);
            }
        }

        private static void RemoveBallComboAtColumn(int column, GridTileDisplay[,] baseGrid, Guid[,] ballGrid,
            Vector3 endPosition, ICollection<BallDisplay> ballPool, IReadOnlyList<BallType> columnColors)
        {
            var columnColor = columnColors[column];
            // Move each ball with target color out of grid until a different ball is present 
            for (var y = 0; y < ballGrid.GetLength(1); y++)
            {
                var ballId = ballGrid[column, y];
                // Combos can only work from bottom up without interruptions
                if (ballId == Guid.Empty) break;
                var ball = GetBallFromPoolById(ballPool, ballId);

                // Stop iteration on a different (regular) ball
                if (ball.Type != BallType.Special && ball.Type != columnColor) break;
                // Stop iteration if block ball is present
                if (ball.Type == BallType.Special && ball is BlockBall) break;

                MainLevelController.Instance.CurrentScore += 10 * (y + 1);

                // Move ball towards end tile
                var path = Enumerable.Range(0, y + 1)
                    .Select(i => baseGrid[column, i].transform.position).Reverse()
                    .ToList();
                path.Add(endPosition);
                ball.MoveOnPathOverTime(path, 0.1f, b =>
                {
                    ballPool.Remove(b);
                    Destroy(b.gameObject);
                    if (ballPool.Count == 0)
                    {
                        MainLevelController.Instance.LevelState = LevelState.EndLevelWin;
                    }
                });
                ballGrid[column, y] = Guid.Empty;
            }
        }

        private static void MoveBallsToBottomAtColumn(int column, GridTileDisplay[,] baseGrid, Guid[,] ballGrid,
            IReadOnlyCollection<BallDisplay> ballPool)
        {
            for (var y = 1; y < ballGrid.GetLength(1); y++)
            {
                var ballId = ballGrid[column, y];
                if (ballId == Guid.Empty) continue;
                var ball = GetBallFromPoolById(ballPool, ballId);

                // Determine step size individually in order to avoid blank spots
                var stepSize = 0;
                for (var i = y - 1; i >= 0; i--)
                {
                    if (ballGrid[column, i] == Guid.Empty)
                    {
                        stepSize++;
                    }
                }

                if (stepSize == 0) continue;

                // Move ball down
                var path = Enumerable.Range(y - stepSize, stepSize).Reverse()
                    .Select(i => baseGrid[column, i].transform.position)
                    .ToList();
                ball.MoveOnPathOverTime(path, 0.1f);
                ballGrid[column, y] = Guid.Empty;
                ballGrid[column, y - stepSize] = ball.ID;
                ball.GridPosition = new Vector2Int(column, y - stepSize);
                ball.WorldOriginPosition = path.Last();
            }
        }

        private static BallDisplay GetBallFromPoolById(IEnumerable<BallDisplay> pool, Guid id)
        {
            return pool.FirstOrDefault(b => b.ID == id);
        }
    }
}