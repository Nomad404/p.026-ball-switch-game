using UnityEngine;
using UnityEngine.SceneManagement;

namespace BallSwitchGame.Controllers
{
    internal class GameController : MonoBehaviour
    {
        public static GameController Instance;

        private void Awake()
        {
            if (!Instance)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
                SceneManager.sceneLoaded += OnSceneLoaded;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private static void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            GameTime.IsPaused = false;
        }

        public void LoadMainGame()
        {
            SceneManager.LoadScene(LevelNames.MainGame);
            SceneManager.LoadScene(LevelNames.MainGameUI, LoadSceneMode.Additive);
        }

        public void LoadStartMenu()
        {
            SceneManager.LoadScene(LevelNames.StartMenu);
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }

    public static class LevelNames
    {
        public const string StartMenu = "StartMenu";
        public const string MainGame = "MainGame";
        public const string MainGameUI = "MainGameUI";
    }

    public static class GameTime
    {
        private static bool _isPaused;

        public static bool IsPaused
        {
            get => _isPaused;
            set
            {
                _isPaused = value;
                Time.timeScale = _isPaused ? 0f : 1f;
            }
        }
    }
}