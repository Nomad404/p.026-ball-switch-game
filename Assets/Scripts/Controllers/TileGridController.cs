using System.Linq;
using System.Runtime.CompilerServices;
using BallSwitchGame.Dynamic;
using BallSwitchGame.Grid;
using BallSwitchGame.Static;
using UnityEditor;
using UnityEngine;

[assembly: InternalsVisibleTo("BallSwitchGame.Editor")]

namespace BallSwitchGame.Controllers
{
    internal class TileGridController : MonoBehaviour
    {
        private static TileGridController _instance;

        public static TileGridController Instance
        {
            get
            {
                if (_instance == null) _instance = FindObjectOfType<TileGridController>();
                return _instance;
            }
        }

        [Header("Grid settings")] public Vector2Int gridSize;
        [SerializeField] private Vector2 tileOffset;
        [SerializeField] private float startTileOffset;
        [SerializeField] private float endTileOffset;

        [Header("Objects")] public GridTileDisplay tileDisplayDisplayPrefab;
        public GridTileDisplay tileDisplayColumnColorPrefab;

        [SerializeField] private Transform tileGridContainer;

        [Header("Column colors")] public BallType[] columnColors;

        [Space] public bool autoUpdate;

        public Grid.Grid Grid { get; private set; }

        public GridTileDisplay StartTileDisplay { get; private set; }
        public GridTileDisplay EndTileDisplay { get; private set; }

        private void OnValidate()
        {
            gridSize = new Vector2Int(Mathf.Clamp(gridSize.x, 2, 15), Mathf.Clamp(gridSize.y, 2, 15));
            tileOffset = new Vector2(Mathf.Clamp(tileOffset.x, 0, 5), Mathf.Clamp(tileOffset.y, 0, 5));

            if (columnColors == null || columnColors.Length != gridSize.x)
            {
                var temp = columnColors;
                columnColors = new BallType[gridSize.x];
                if (temp == null) return;
                var endIndex = temp.Length > gridSize.x ? gridSize.x - 1 : temp.Length - 1;
                temp[..endIndex].CopyTo(columnColors, 0);
            }
        }

        private void Awake()
        {
            MainLevelController.Instance.OnNewLevelState += state =>
            {
                if (state != LevelState.SetupGrid) return;
                GenerateFullGrid();
                MainLevelController.Instance.LevelState = LevelState.SetupQueue;
            };
        }

        public void GenerateFullGrid()
        {
            ClearGrid();
            SetupGrid();
        }

        public void ClearGrid()
        {
            GetComponentsInChildren<GridTileDisplay>().ToList().ForEach(tile =>
            {
                // Necessary for running within the editor
#if UNITY_EDITOR
                if (!EditorApplication.isPlaying)
                {
                    DestroyImmediate(tile.gameObject);
                }
                else
                {
                    Destroy(tile.gameObject);
                }
#else
                Destroy(tile.gameObject);
#endif
            });
            Grid = null;
        }

        private void SetupGrid()
        {
            var gridHalfWidth = gridSize.x / 2;
            var gridHalfHeight = gridSize.y / 2;
            var scaledTileSizeX = tileDisplayDisplayPrefab.TileSize.x * (1 + tileOffset.x);
            var scaledTileSizeY = tileDisplayDisplayPrefab.TileSize.y * (1 + tileOffset.y);

            // Setup grid tiles
            Grid = new Grid.Grid(gridSize, gridPos =>
            {
                var pos = new Vector3((gridPos.x - gridHalfWidth) * scaledTileSizeX + scaledTileSizeX / 2,
                    (gridPos.y - gridHalfHeight) * scaledTileSizeY + scaledTileSizeY / 2, 0);
                var tile = Instantiate(tileDisplayDisplayPrefab, tileGridContainer);
                tile.transform.localPosition = pos;
                tile.name = $"GridTile #{gridPos.x * gridPos.y} ({gridPos.x}|{gridPos.y})";
                tile.SortingOrder = SpriteLayers.GridTiles;
                return tile;
            });

            // Setup color tiles for columns
            Vector3 pos;
            var scaledColorTileSizeX = tileDisplayColumnColorPrefab.TileSize.x * (1 + tileOffset.x);
            var scaledColorTileSizeY = tileDisplayColumnColorPrefab.TileSize.y * (1 + tileOffset.y);
            for (var x = 0; x < gridSize.x; x++)
            {
                var color = BallDisplay.GetBallColorByType(columnColors[x]);
                color.a = tileDisplayColumnColorPrefab.TileColor.a;
                pos = new Vector3((x - gridHalfWidth) * scaledColorTileSizeX + scaledColorTileSizeX / 2,
                    -gridHalfHeight * scaledTileSizeY - scaledColorTileSizeY / 2, 0);
                var tile = Instantiate(tileDisplayColumnColorPrefab, tileGridContainer);
                tile.transform.localPosition = pos;
                tile.TileColor = color;
                tile.name = $"ColorTile {columnColors[x]}";
            }

            // Setup start & end tile
            pos = new Vector3(0, gridHalfHeight * scaledTileSizeY + scaledTileSizeY / 2 * (1 + startTileOffset), 0);
            StartTileDisplay = Instantiate(tileDisplayDisplayPrefab, tileGridContainer);
            StartTileDisplay.transform.localPosition = pos;
            StartTileDisplay.name = "StartTile";
            pos = new Vector3(0,
                -gridHalfHeight * scaledTileSizeY - scaledTileSizeY / 2 * (1 + endTileOffset) - scaledColorTileSizeY,
                0);
            EndTileDisplay = Instantiate(tileDisplayDisplayPrefab, tileGridContainer);
            EndTileDisplay.transform.localPosition = pos;
            EndTileDisplay.name = "EndTile";

            StartTileDisplay.SortingOrder = EndTileDisplay.SortingOrder = SpriteLayers.GridTiles;
        }
    }
}