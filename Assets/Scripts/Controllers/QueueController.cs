using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using BallSwitchGame.Dynamic;
using BallSwitchGame.Grid;
using BallSwitchGame.Static;
using UnityEditor;
using UnityEngine;

[assembly: InternalsVisibleTo("BallSwitchGame.Editor")]

namespace BallSwitchGame.Controllers
{
    internal class QueueController : MonoBehaviour
    {
        private static QueueController _instance;

        public static QueueController Instance
        {
            get
            {
                if (_instance == null) _instance = FindObjectOfType<QueueController>();
                return _instance;
            }
        }
        
        [Header("Setup"), SerializeField] private List<Transform> queuePathCorners = new();
        [SerializeField] private float pathTileMargin;
        [SerializeField] private bool includeSpecialsOnStartup;

        private readonly List<GridTileDisplay> _queueTiles = new();
        public Queue<BallDisplay> BallQueue { get; } = new();

        private IEnumerator _queueMoveEnumerator;

        private const float QueueTimerMinTime = 3f;
        private const float QueueTimerMaxTime = 15f;
        
        [Header("Queue timer"), Range(QueueTimerMinTime, QueueTimerMaxTime), SerializeField]
        private float ballQueueRestartTime = 5f;

        [SerializeField, Range(0.1f, 1f)] private float queueRestartDecreaseStep = 0.3f;
        private float ScaledRestartTime => ballQueueRestartTime * 1000;

        [SerializeField] private Transform timerSprite;

        public readonly Stopwatch BallQueueTimer = new();

        [Space] public bool autoUpdate;

        private void Awake()
        {
            MainLevelController.Instance.OnNewLevelState += newState =>
            {
                switch (newState)
                {
                    case LevelState.SetupQueue:
                        ClearQueueTiles();
                        CreateQueueTiles();
                        MainLevelController.Instance.LevelState = LevelState.SetupBalls;
                        break;
                    case LevelState.SetupBalls:
                        StartCoroutine(SetupBalls());
                        break;
                    case LevelState.StartLevel:
                        BallQueueTimer.Start();
                        break;
                    case LevelState.EndLevelLose:
                    case LevelState.EndLevelWin:
                        BallQueueTimer.Stop();
                        break;
                }
            };
        }

        private void Update()
        {
            if (BallQueueTimer.IsRunning && BallQueueTimer.ElapsedMilliseconds > ScaledRestartTime)
            {
                EnqueueRandomBall(true);
                ballQueueRestartTime = Mathf.Clamp(ballQueueRestartTime - queueRestartDecreaseStep, QueueTimerMinTime,
                    QueueTimerMaxTime);
                BallQueueTimer.Restart();
            }

            timerSprite.transform.localScale =
                new Vector3(1, BallQueueTimer.ElapsedMilliseconds / ScaledRestartTime, 1);
        }

        public void CreateQueueTiles()
        {
            var pathPairs = GetPathPairs(queuePathCorners);
            if (pathPairs.Count == 0) return;
            _queueTiles.Clear();
            var totalTileCount = 0;
            foreach (var pair in pathPairs)
            {
                // TODO: Switch to sqrMagnitude somehow
                var distance = Vector3.Distance(pair.a.position, pair.b.position);
                var tileDiameter = TileGridController.Instance.tileDisplayDisplayPrefab.TileSize.x;
                var tileCountForPair = (int) (distance / (tileDiameter + pathTileMargin));
                if (tileCountForPair == 0) continue;
                for (var j = 0; j < tileCountForPair; j++)
                {
                    var ratio = (float) j / tileCountForPair;
                    var pos = Vector3.Lerp(pair.a.position, pair.b.position, ratio);
                    var tile = Instantiate(TileGridController.Instance.tileDisplayDisplayPrefab, transform);
                    tile.transform.localPosition = pos;
                    tile.SortingOrder = SpriteLayers.QueueTiles;
                    tile.name = $"Queue tile #{totalTileCount}";
                    totalTileCount++;
                    _queueTiles.Add(tile);
                }
            }
        }

        public void ClearQueueTiles()
        {
            GetComponentsInChildren<GridTileDisplay>().ToList().ForEach(tile =>
            {
                // Necessary for running within the editor
#if UNITY_EDITOR
                if (!EditorApplication.isPlaying)
                {
                    DestroyImmediate(tile.gameObject);
                }
                else
                {
                    Destroy(tile.gameObject);
                }
#else
                Destroy(tile.gameObject);
#endif
            });
        }

        private IEnumerator SetupBalls()
        {
            // Small pause for dramatic effect
            yield return new WaitForSeconds(0.5f);

            var gridSize = TileGridController.Instance.gridSize;
            var ballGridController = BallGridController.Instance;
            var ballCount = gridSize.x * gridSize.y + 5;
            for (var i = 0; i < ballCount; i++)
            {
                EnqueueRandomBall(includeSpecialsOnStartup);
                yield return new WaitForSeconds(0.3f);
            }

            yield return new WaitWhile(() => ballGridController.GetFreeColumnIndices().Count > 0);
            MainLevelController.Instance.LevelState = LevelState.StartLevel;
        }

        private void EnqueueRandomBall(bool includeSpecials = false)
        {
            if (BallQueue.Count == _queueTiles.Count)
            {
                MainLevelController.Instance.LevelState = LevelState.EndLevelLose;
                return;
            }

            var ball = BallGridController.Instance.CreateRandomBall(includeSpecials);
            EnqueueBall(ball);
        }

        private void EnqueueBall(BallDisplay ball)
        {
            if (BallQueue.Count == _queueTiles.Count)
            {
                MainLevelController.Instance.LevelState = LevelState.EndLevelLose;
                return;
            }

            ball.SortingOrder = SpriteLayers.QueueBalls;
            ball.transform.position = _queueTiles.Last().transform.position;
            ball.QueueIndex = _queueTiles.Count - 1;

            var startIndex = BallQueue.Count;
            var pathStepCount = ball.QueueIndex - startIndex;
            var path = _queueTiles.GetRange(startIndex, pathStepCount).Select(tile => tile.transform.position)
                .Reverse();
            BallQueue.Enqueue(ball);

            ball.QueueIndex = startIndex;
            ball.MoveOnPathOverTime(path, 0.05f, b =>
            {
                if (b.QueueIndex != 0) return;
                var freeXPositions = BallGridController.Instance.GetFreeColumnIndices();
                if (freeXPositions.Count == 0) return;
                StartCoroutine(DequeueBall());
            });
        }

        private IEnumerator DequeueBall()
        {
            var latestBall = BallQueue.Dequeue();
            BallGridController.Instance.AddBallToGrid(latestBall);
            var currentQueue = BallQueue.ToArray();
            foreach (var ball in currentQueue)
            {
                while (ball.IsMoving)
                {
                    yield return null;
                }

                ball.QueueIndex--;
                var startIndex = ball.QueueIndex;
                var pathStepCount = ball.QueueIndex + 1;
                var path = _queueTiles.GetRange(startIndex, pathStepCount).Select(tile => tile.transform.position)
                    .Reverse();
                ball.MoveOnPathOverTime(path, 0.05f, b =>
                {
                    if (b.QueueIndex != 0) return;
                    var freeXPositions = BallGridController.Instance.GetFreeColumnIndices();
                    if (freeXPositions.Count == 0) return;
                    StartCoroutine(DequeueBall());
                });
            }
        }

        public void MoveUpQueuedBalls()
        {
            var indices = BallGridController.Instance.GetFreeColumnIndices();
            if (indices.Count > 0 && BallQueue.Count > 0)
            {
                StartCoroutine(DequeueBall());
            }
        }

        private static List<(Transform a, Transform b)> GetPathPairs(IReadOnlyList<Transform> original)
        {
            var result = new List<(Transform a, Transform b)>();
            if (original.Count <= 1) return result;
            for (var i = 0; i < original.Count - 1; i++)
            {
                var a = original[i];
                var b = original[i + 1];
                result.Add((a, b));
            }

            return result;
        }
    }
}