using UnityEngine;
using UnityEngine.UI;

namespace BallSwitchGame.Controllers
{
    internal class StartMenuController : MonoBehaviour
    {
        [SerializeField] private Button quitButton;

        private GameController _gameController;

        private void Awake()
        {
            _gameController = GameController.Instance;

            // Necessary since there is no quit function on iOS
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                quitButton.gameObject.SetActive(false);
            }
        }

        public void StartGame()
        {
            GameController.Instance.LoadMainGame();
        }

        public void QuitGame()
        {
            _gameController.QuitGame();
        }
    }
}