using System;
using UnityEngine;

namespace BallSwitchGame.Controllers
{
    internal class MainLevelController : MonoBehaviour
    {
        private static MainLevelController _instance;

        public static MainLevelController Instance
        {
            get
            {
                if (_instance == null) _instance = FindObjectOfType<MainLevelController>();
                return _instance;
            }
        }

        private LevelState _levelState;
        private int _currentScore;

        public LevelState LevelState
        {
            get => _levelState;
            set
            {
                if (_levelState == value) return;
                _levelState = value;
                OnNewLevelState?.Invoke(_levelState);
            }
        }

        public int CurrentScore
        {
            get => _currentScore;
            set
            {
                if (_currentScore == value) return;
                _currentScore = value;
                OnNewScore?.Invoke(_currentScore);
            }
        }

        public event Action<LevelState> OnNewLevelState;
        public event Action<int> OnNewScore;

        private void Start()
        {
            LevelState = LevelState.SetupGrid;
        }
    }

    public enum LevelState
    {
        Default = 0,
        SetupGrid = 1,
        SetupQueue = 3,
        SetupBalls = 4,
        StartLevel = 5,
        EndLevelLose = 6,
        EndLevelWin = 7
    }

    public class WaitForLevelState : CustomYieldInstruction
    {
        public override bool keepWaiting => _levelController.LevelState != _checkState;

        private readonly MainLevelController _levelController;
        private readonly LevelState _checkState;

        public WaitForLevelState(LevelState state)
        {
            _levelController = MainLevelController.Instance;
            _checkState = state;
        }
    }
}