using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BallSwitchGame.Controllers
{
    internal class UIController : MonoBehaviour
    {
        [Header("Start screen"), SerializeField]
        private RectTransform startScreenContainer;

        [Header("End screen"), SerializeField] private RectTransform endScreenContainer;
        [SerializeField] private TextMeshProUGUI endScreenHeader;

        [Space, SerializeField] private RectTransform pauseMenuContainer;
        [SerializeField] private List<Button> quitButtons;

        private void Awake()
        {
            // Necessary since there is no quit function on iOS
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                quitButtons.ForEach(b => b.gameObject.SetActive(false));
            }

            MainLevelController.Instance.OnNewLevelState += state =>
            {
                switch (state)
                {
                    case LevelState.StartLevel:
                        startScreenContainer.gameObject.SetActive(false);
                        break;
                    case LevelState.EndLevelWin:
                        endScreenContainer.gameObject.SetActive(true);
                        endScreenHeader.text = "You win!";
                        GameTime.IsPaused = true;
                        break;
                    case LevelState.EndLevelLose:
                        endScreenContainer.gameObject.SetActive(true);
                        endScreenHeader.text = "Game over!";
                        GameTime.IsPaused = true;
                        break;
                }
            };
        }

        public void OpenPauseMenu()
        {
            pauseMenuContainer.gameObject.SetActive(true);
            GameTime.IsPaused = true;
        }

        public void ClosePauseMenu()
        {
            pauseMenuContainer.gameObject.SetActive(false);
            GameTime.IsPaused = false;
        }

        public void RestartLevel()
        {
            GameController.Instance.LoadMainGame();
        }

        public void ReturnToMainMenu()
        {
            GameController.Instance.LoadStartMenu();
        }

        public void QuitGame()
        {
            GameController.Instance.QuitGame();
        }
    }
}