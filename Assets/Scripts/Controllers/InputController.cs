using System;
using UnityEngine;

namespace BallSwitchGame.Controllers
{
    public class InputController : MonoBehaviour
    {
        private static InputController _instance;

        public static InputController Instance
        {
            get
            {
                if (_instance == null) _instance = FindObjectOfType<InputController>();
                return _instance;
            }
        }

        [SerializeField] public Camera mainCamera;

        private (InputType type, Vector3 position) _latestTouchEvent;
        private (InputType type, Vector3 position) _latestMoueEvent;

        private (InputType type, Vector3 position) LatestTouchEvent
        {
            set
            {
                if (_latestTouchEvent == value) return;
                _latestTouchEvent = value;
                OnNewTouchEvent?.Invoke(_latestTouchEvent.type, _latestTouchEvent.position);
            }
        }

        private (InputType type, Vector3 position) LatestMouseEvent
        {
            set
            {
                if (_latestMoueEvent == value) return;
                _latestMoueEvent = value;
                OnNewMouseEvent?.Invoke(_latestMoueEvent.type, _latestMoueEvent.position);
            }
        }

        public event Action<InputType, Vector3> OnNewTouchEvent;
        public event Action<InputType, Vector3> OnNewMouseEvent;

        private MainLevelController _levelController;

        public Guid InputLockTarget { get; set; }

        private void Awake()
        {
            if (mainCamera == null) throw new NullReferenceException("Main camera must not be null");
            _levelController = MainLevelController.Instance;
        }

        private void Update()
        {
            if (_levelController.LevelState == LevelState.StartLevel)
            {
                CheckTouchInput();
                CheckMouseInput();
            }
        }

        private void CheckTouchInput()
        {
            if (Input.touchCount <= 0) return;
            var touch = Input.GetTouch(0);
            var position = mainCamera.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, 0));
            position.z = 0f;
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    LatestTouchEvent = (InputType.Start, position);
                    break;
                case TouchPhase.Moved:
                    LatestTouchEvent = (InputType.Drag, position);
                    break;
                case TouchPhase.Canceled:
                case TouchPhase.Ended:
                    LatestTouchEvent = (InputType.End, position);
                    break;
            }
        }

        private void CheckMouseInput()
        {
            var inputType = InputType.Default;
            if (Input.GetMouseButtonDown(0))
            {
                inputType = InputType.Start;
            }
            else if (Input.GetMouseButton(0))
            {
                inputType = InputType.Drag;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                inputType = InputType.End;
            }

            if (inputType == InputType.Default) return;
            var mousePos = Input.mousePosition;
            var position = mainCamera.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 0));
            position.z = 0f;
            LatestMouseEvent = (inputType, position);
        }

        public enum InputType
        {
            Default,
            Start,
            Drag,
            End
        }
    }
}