using BallSwitchGame.Controllers;
using UnityEditor;
using UnityEngine;

namespace BallSwitchGame.Editor
{
    [CustomEditor(typeof(QueueController))]
    internal class QueueControllerEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var controller = (QueueController)target;
            if (DrawDefaultInspector())
            {
                if (controller.autoUpdate)
                {
                    controller.ClearQueueTiles();
                    controller.CreateQueueTiles();
                }
            }

            EditorGUILayout.Space();
            DrawDefaultButtons(controller);
        }

        private void DrawDefaultButtons(QueueController controller)
        {
            if (GUILayout.Button("Clear queue tiles"))
            {
                controller.ClearQueueTiles();
            }

            if (GUILayout.Button("Generate queue tiles"))
            {
                controller.CreateQueueTiles();
            }
        }
    }
}