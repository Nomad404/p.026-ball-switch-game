using BallSwitchGame.Controllers;
using UnityEditor;
using UnityEngine;

namespace BallSwitchGame.Editor
{
    [CustomEditor(typeof(TileGridController))]
    internal class TileGridControllerEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var controller = (TileGridController)target;
            if (DrawDefaultInspector())
            {
                if (controller.autoUpdate)
                {
                    controller.ClearGrid();
                    controller.GenerateFullGrid();
                }
            }

            EditorGUILayout.Space();
            DrawDefaultButtons(controller);
        }

        private static void DrawDefaultButtons(TileGridController controller)
        {
            if (GUILayout.Button("Clear grid"))
            {
                controller.ClearGrid();
            }

            if (GUILayout.Button("Generate grid"))
            {
                controller.GenerateFullGrid();
            }
        }
    }
}