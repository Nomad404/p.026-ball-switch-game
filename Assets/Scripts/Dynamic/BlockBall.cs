using UnityEngine;

namespace BallSwitchGame.Dynamic
{
    /// <summary>
    /// Class itself does nothing special, only necessary for type comparison
    /// </summary>
    internal class BlockBall : BallDisplay
    {
        [Header("Sprites"), SerializeField] private SpriteRenderer crossSprite;

        protected override void UpdateSortingOrder()
        {
            base.UpdateSortingOrder();
            crossSprite.sortingOrder = SortingOrder + 1;
        }
    }
}