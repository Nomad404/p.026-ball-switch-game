using System.Diagnostics;
using BallSwitchGame.Controllers;
using UnityEngine;

namespace BallSwitchGame.Dynamic
{
    internal class BombBall : BallDisplay
    {
        [Header("Sprites"), SerializeField] private SpriteRenderer bombSprite;
        [SerializeField] private SpriteRenderer timerSprite;

        [Header("Properties"), SerializeField, Range(3f, 30f)]
        private float timeUntilExplosion = 20f;

        private readonly Stopwatch _laserTimer = new();

        public override Vector2Int GridPosition
        {
            get => base.GridPosition;
            set
            {
                if (value.x >= 0 || value.y >= 0) _laserTimer.Start();
                base.GridPosition = value;
            }
        }

        private void Update()
        {
            if (_laserTimer.IsRunning)
            {
                if (_laserTimer.ElapsedMilliseconds > timeUntilExplosion * 1000f)
                {
                    _laserTimer.Stop();
                    Explode();
                }

                timerSprite.transform.localScale =
                    new Vector3(1, _laserTimer.ElapsedMilliseconds / (timeUntilExplosion * 1000f));
            }
            else
            {
                timerSprite.transform.localScale = new Vector3(1, 0, 1);
            }
        }

        private new void OnDestroy()
        {
            base.OnDestroy();
            _laserTimer.Stop();
        }

        private void Explode()
        {
            BallGridController.Instance.DestroyBlockAt(GridPosition);
        }

        protected override void UpdateSortingOrder()
        {
            base.UpdateSortingOrder();
            bombSprite.sortingOrder = SortingOrder + 1;
            timerSprite.sortingOrder = SortingOrder + 2;
        }
    }
}