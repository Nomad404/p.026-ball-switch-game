using System.Collections.Generic;
using BallSwitchGame.Controllers;
using UnityEngine;

namespace BallSwitchGame.Dynamic
{
    internal class NoSwitchBall : BallDisplay
    {
        [Header("Sprites"), SerializeField] private List<SpriteRenderer> arrowSprites;
        [SerializeField] private SpriteRenderer lockSprite;

        private new void OnDestroy()
        {
            // Do nothing
        }

        protected override void OnNewInputEvent(InputController.InputType inputType, Vector3 position)
        {
            // Do nothing
        }

        protected override void UpdateSortingOrder()
        {
            base.UpdateSortingOrder();
            arrowSprites.ForEach(sprite => sprite.sortingOrder = SortingOrder + 1);
            lockSprite.sortingOrder = SortingOrder + 2;
        }
    }
}