using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BallSwitchGame.Controllers;
using BallSwitchGame.Static;
using UnityEngine;

namespace BallSwitchGame.Dynamic
{
    /// <summary>
    /// Visual representation of a ball
    /// </summary>
    [RequireComponent(typeof(SpriteRenderer))]
    public class BallDisplay : MonoBehaviour
    {
        private const float MoveDistanceThreshold = 0.00001f;

        [SerializeField] protected BallType type = BallType.Default;

        public BallType Type
        {
            get => type;
            set
            {
                if (type == value) return;
                type = value;
                var color = GetBallColorByType(type);
                color.a = GetSpriteAlpha();
                _baseSpriteRenderer.color = color;
            }
        }

        public Guid ID { get; private set; }

        public Vector2Int GridSize { get; set; }

        // Grid coordinate position
        private Vector2Int _gridPosition = new(-1, -1);

        public virtual Vector2Int GridPosition
        {
            get => _gridPosition;
            set
            {
                if (value == _gridPosition) return;
                _gridPosition = value;
                var tempColor = _baseSpriteRenderer.color;
                tempColor.a = GetSpriteAlpha();
                _baseSpriteRenderer.color = tempColor;
            }
        }

        // Redundant position on grid to return to
        public Vector3 WorldOriginPosition { get; set; }

        public int QueueIndex { get; set; } = -1;

        private int _sortingOrder = SpriteLayers.Balls;

        public int SortingOrder
        {
            get => _sortingOrder;
            set
            {
                if (value == _sortingOrder) return;
                _sortingOrder = value;
                UpdateSortingOrder();
            }
        }

        public Vector3 BallSize => GetComponent<Renderer>().bounds.size;

        private InputController _inputController;

        public bool IsMoving => _moveEnumerator != null;
        private IEnumerator _moveEnumerator;
        private SpriteRenderer _baseSpriteRenderer;

        private void Awake()
        {
            ID = Guid.NewGuid();
            _baseSpriteRenderer = GetComponent<SpriteRenderer>();
            UpdateSortingOrder();

            _inputController = InputController.Instance;
            _inputController.OnNewTouchEvent += OnNewInputEvent;
            _inputController.OnNewMouseEvent += OnNewInputEvent;
        }

        protected void OnDestroy()
        {
            _inputController.OnNewTouchEvent -= OnNewInputEvent;
            _inputController.OnNewMouseEvent -= OnNewInputEvent;
        }

        protected virtual void OnNewInputEvent(InputController.InputType inputType, Vector3 position)
        {
            if (_inputController.InputLockTarget != Guid.Empty && _inputController.InputLockTarget != ID) return;
            if (GridPosition.x < 0 || GridPosition.y < 0) return;
            var distanceToPoint = (position - transform.position).sqrMagnitude;
            var bounds = (Vector3.up * (BallSize.x / 2)).sqrMagnitude;
            if (distanceToPoint > bounds && _inputController.InputLockTarget != ID) return;
            switch (inputType)
            {
                case InputController.InputType.Start:
                    OnBallStartDrag();
                    break;
                case InputController.InputType.Drag:
                    OnBallDrag(position);
                    break;
                case InputController.InputType.End:
                    OnBallStopDrag();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(inputType), inputType, null);
            }
        }

        private void OnBallStartDrag()
        {
            _inputController.InputLockTarget = ID;
            _baseSpriteRenderer.sortingOrder = SpriteLayers.Balls + 1;
        }

        private void OnBallDrag(Vector3 worldPos)
        {
            // Lock down ball position on grid
            var minX = GridPosition.x == 0 ? WorldOriginPosition.x : WorldOriginPosition.x - BallSize.x;
            var maxX = GridPosition.x == GridSize.x - 1 ? WorldOriginPosition.x : WorldOriginPosition.x + BallSize.x;
            transform.position = new Vector3(Mathf.Clamp(worldPos.x, minX, maxX), WorldOriginPosition.y,
                WorldOriginPosition.z);
        }

        private void OnBallStopDrag()
        {
            _inputController.InputLockTarget = Guid.Empty;
            _baseSpriteRenderer.sortingOrder = SpriteLayers.Balls;
            var distanceToOrigin = (WorldOriginPosition - transform.position).sqrMagnitude;
            var bounds = (Vector3.up * (BallSize.x / 2)).sqrMagnitude;
            if (distanceToOrigin > bounds)
            {
                var gridPosX = GridPosition.x;
                var toRight = transform.position.x > WorldOriginPosition.x;
                var target = new Vector2Int(toRight ? gridPosX + 1 : gridPosX - 1,
                    GridPosition.y);
                var success = BallGridController.Instance.MoveBallOnGridTo(GridPosition, target);
                if (!success) transform.position = WorldOriginPosition;
            }
            else
            {
                transform.position = WorldOriginPosition;
            }
        }

        public void MoveOnPathOverTime(IEnumerable<Vector3> path, float durationPerMove,
            Action<BallDisplay> finishCallback = null)
        {
            if (_moveEnumerator != null) StopCoroutine(_moveEnumerator);
            _moveEnumerator = Seq_MoveOnPathOverTime(path, durationPerMove, finishCallback);
            StartCoroutine(_moveEnumerator);
        }

        public void MoveToOverTime(Vector3 destination, float timeFrame, Action<BallDisplay> finishCallback = null)
        {
            if (_moveEnumerator != null) StopCoroutine(_moveEnumerator);
            _moveEnumerator = Seq_MoveToOverTime(destination, timeFrame, finishCallback);
            StartCoroutine(_moveEnumerator);
        }

        private IEnumerator Seq_MoveOnPathOverTime(IEnumerable<Vector3> path, float durationPerMove,
            Action<BallDisplay> finishCallback = null)
        {
            yield return path.Select(destination => Seq_MoveToOverTime(destination, durationPerMove)).GetEnumerator();

            if (finishCallback == null) yield break;
            _moveEnumerator = null;
            finishCallback(this);
        }

        private IEnumerator Seq_MoveToOverTime(Vector3 destination, float timeFrame,
            Action<BallDisplay> finishCallback = null)
        {
            var startPos = transform.position;
            var t = 0f;
            while ((destination - transform.position).sqrMagnitude > MoveDistanceThreshold * MoveDistanceThreshold)
            {
                t += Time.fixedDeltaTime / timeFrame;
                transform.position = Vector3.Lerp(startPos, destination, t);
                yield return new WaitForFixedUpdate();
            }

            if (finishCallback == null) yield break;
            _moveEnumerator = null;
            finishCallback(this);
        }

        public static Color GetBallColorByType(BallType type)
        {
            return type switch
            {
                BallType.Default => Color.white,
                BallType.Red => Color.red,
                BallType.Green => Color.green,
                BallType.Blue => Color.blue,
                BallType.Yellow => Color.yellow,
                BallType.Special => Color.magenta,
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        protected virtual void UpdateSortingOrder()
        {
            if (_baseSpriteRenderer == null) _baseSpriteRenderer = GetComponent<SpriteRenderer>();
            _baseSpriteRenderer.sortingOrder = SortingOrder;
        }

        private float GetSpriteAlpha()
        {
            return _gridPosition.x < 0 || _gridPosition.y < 0 ? 0.5f : 1f;
        }
    }

    public enum BallType
    {
        Default = 0,
        Red = 1,
        Green = 2,
        Blue = 3,
        Yellow = 4,
        Special = 5
    }
}