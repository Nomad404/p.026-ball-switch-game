using System.Threading.Tasks;
using BallSwitchGame.Controllers;
using UnityEngine;

namespace BallSwitchGame.Dynamic
{
    internal class FreezeBall : BallDisplay
    {
        [SerializeField, Range(1f, 8f)] private float freezeSeconds = 3f;
        [SerializeField] private SpriteRenderer snowflakeSprite;

        private new async void OnDestroy()
        {
            base.OnDestroy();
            var timer = QueueController.Instance.BallQueueTimer;
            timer.Stop();
            await Task.Delay((int) (freezeSeconds * 1000));
            timer.Start();
        }

        protected override void UpdateSortingOrder()
        {
            base.UpdateSortingOrder();
            snowflakeSprite.sortingOrder = SortingOrder + 1;
        }
    }
}