using UnityEngine;

namespace BallSwitchGame.UI
{
    /// <summary>
    /// Moves a rect away from the safe area of a phone
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    public class UISafeAreaMover : MonoBehaviour
    {
        private RectTransform _rect;

        private void Awake()
        {
            _rect = GetComponent<RectTransform>();
            var newPos = CheckSafeAreaBounds(_rect);
            _rect.transform.position = newPos;
        }

        private static Vector3 CheckSafeAreaBounds(RectTransform rect)
        {
            var resultPos = rect.position;
            var bounds = rect.rect;
            var safeArea = Screen.safeArea;
            if (bounds.yMin < safeArea.yMin) resultPos.y -= safeArea.yMin - bounds.yMin;
            if (bounds.yMax > safeArea.yMax) resultPos.y += bounds.yMax - safeArea.yMax;
            if (bounds.xMin < safeArea.xMin) resultPos.x -= safeArea.xMin - bounds.xMin;
            if (bounds.xMax > safeArea.xMax) resultPos.x += bounds.xMax - safeArea.xMax;
            return resultPos;
        }
    }
}