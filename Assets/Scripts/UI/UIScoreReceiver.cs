using BallSwitchGame.Controllers;
using TMPro;
using UnityEngine;

namespace BallSwitchGame.UI
{
    /// <summary>
    /// Updates a test according to the current score
    /// </summary>
    [RequireComponent(typeof(TextMeshProUGUI))]
    internal class UIScoreReceiver : MonoBehaviour
    {
        [SerializeField] private bool useNewLine;

        private TextMeshProUGUI _textMesh;

        private void Awake()
        {
            _textMesh = GetComponent<TextMeshProUGUI>();
            MainLevelController.Instance.OnNewScore += UpdateScoreText;
            UpdateScoreText(MainLevelController.Instance.CurrentScore);
        }

        private void UpdateScoreText(int score)
        {
            _textMesh.text = $"Score{(useNewLine ? "\n" : ": ")}{score}";
        }
    }
}